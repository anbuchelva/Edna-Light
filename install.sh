#!/bin/bash

SRC_DIR=$(cd $(dirname $0) && pwd)
ROOT_UID=0

# Destination directory
if [ "$UID" -eq "$ROOT_UID" ]; then
  AURORAE_DIR="/usr/share/aurorae/themes"
  COLORSCHEMES_DIR="/usr/share/color-schemes"
  GLOBAL_DIR="/usr/share/plasma/look-and-feel"
  KONSOLE_DIR="/usr/share/konsole"
  PLASMA_DIR="/usr/share/plasma/desktoptheme"
  WALLPAPER_DIR="/usr/share/wallpapers"
  KVANTUM_DIR="/usr/share/Kvantum"
else
  AURORAE_DIR="$HOME/.local/share/aurorae/themes"
  COLORSCHEMES_DIR="$HOME/.local/share/color-schemes"
  GLOBAL_DIR="$HOME/.local/share/plasma/look-and-feel"
  KONSOLE_DIR="$HOME/.local/share/konsole"
  PLASMA_DIR="$HOME/.local/share/plasma/desktoptheme"
  WALLPAPER_DIR="$HOME/.local/share/wallpapers"
  KVANTUM_DIR="$HOME/.config/Kvantum"
fi

THEME_NAME=Edna-Light

[[ ! -d ${AURORAE_DIR} ]] && mkdir -p ${AURORAE_DIR}
[[ ! -d ${COLORSCHEMES_DIR} ]] && mkdir -p ${COLORSCHEMES_DIR}
[[ ! -d ${GLOBAL_DIR} ]] && mkdir -p ${GLOBAL_DIR}
[[ ! -d ${KONSOLE_DIR} ]] && mkdir -p ${KONSOLE_DIR}
[[ ! -d ${PLASMA_DIR} ]] && mkdir -p ${PLASMA_DIR}
[[ ! -d ${WALLPAPER_DIR} ]] && mkdir -p ${WALLPAPER_DIR}
[[ ! -d ${KVANTUM_DIR} ]] && mkdir -p ${KVANTUM_DIR}

install() {
  local name=${1}

  cp -rf ${SRC_DIR}/Aurorae/* ${AURORAE_DIR}
  cp -rf ${SRC_DIR}/Color-schemes/*.colors ${COLORSCHEMES_DIR}
  cp -rf ${SRC_DIR}/Look-and-feel/* ${GLOBAL_DIR}
  cp -rf ${SRC_DIR}/Konsole/* ${KONSOLE_DIR}
  cp -rf ${SRC_DIR}/${THEME_NAME} ${PLASMA_DIR}
  cp -rf ${SRC_DIR}/Wallpaper/* ${WALLPAPER_DIR}
  cp -rf ${SRC_DIR}/Kvantum/* ${KVANTUM_DIR}
}

echo "Installing the ${THEME_NAME} theme for the Plasma Desktop..."

install "${name:-${THEME_NAME}}"

echo "Install finished..."
